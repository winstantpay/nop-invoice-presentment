﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Widgets.WinstantPayInvoice.Models;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Events;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Media;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.Customers;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Services
{
    public class EventConsumer :
        IConsumer<OrderPlacedEvent>
    {
        private readonly IOrderModelFactory _orderModelFactory;
        private readonly IOrderService _orderService;
        private readonly IPdfService _pdfService;
        private readonly IWorkContext _workContext;
        private readonly OrderSettings _orderSettings;
        private readonly WinstantPayInvoiceSettings _invoiceSettings;
        private readonly WinstantPayHttpClient _winstantPayHttpClient;
        private readonly ILogger _logger;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly WinstantPayInvoiceManager _winstantPayInvoiceManager;
        private readonly IEncryptionService _encryptionService;

        public EventConsumer(
            IOrderModelFactory orderModelFactory,
            IOrderService orderService,
            IPdfService pdfService,
            ILogger logger,
            IWorkContext workContext,
            OrderSettings orderSettings,
            WinstantPayInvoiceSettings invoiceSettings,
            WinstantPayHttpClient winstantPayHttpClient,
            ICustomerAttributeService customerAttributeService,
            IGenericAttributeService genericAttributeService,
            ICustomerAttributeParser customerAttributeParser,
            WinstantPayInvoiceManager winstantPayInvoiceManager,
            IEncryptionService encryptionService)
        {
            _orderModelFactory = orderModelFactory;
            _orderService = orderService;
            _invoiceSettings = invoiceSettings;
            _winstantPayHttpClient = winstantPayHttpClient;
            _logger = logger;
            _pdfService = pdfService;
            _workContext = workContext;
            _orderSettings = orderSettings;
            _customerAttributeService = customerAttributeService;
            _genericAttributeService = genericAttributeService;
            _customerAttributeParser = customerAttributeParser;
            _winstantPayInvoiceManager = winstantPayInvoiceManager;
            _encryptionService = encryptionService;
        }

        /// <summary>
        /// Handle the order placed event
        /// </summary>
        /// <param name="eventMessage">The event message.</param>
        public void HandleEvent(OrderPlacedEvent eventMessage)
        {
            //handle event
            if (_invoiceSettings.IsSendInvoiceToWinstantPayWhenOrderPlaced)
            {
                _winstantPayInvoiceManager.CreateInvoice(eventMessage.Order.Id);
            }
        }

        public void CreateInvoice(int orderId)
        {
            try
            {
                // Decrypt password
                var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
                var loginModel = new WinstantPayLoginModel
                {
                    Username = _invoiceSettings.Username,
                    Password = decryptedPassword,
                    Domain = _invoiceSettings.Domain
                };
                // Login to WinstantPay
                var loginResponse = _winstantPayHttpClient.LoginWinstantPayAsync(_invoiceSettings.WebServiceUrl, loginModel);
                if (loginResponse != null && loginResponse.Result != null)
                {
                    var order = _orderService.GetOrderById(orderId);
                    // Prepare model
                    var model = _orderModelFactory.PrepareOrderModel(null, order);

                    var receiverWpayId = string.Empty;
                    receiverWpayId = GetCustomerWPayID(order.Customer);
                    var receiverCompany = string.Empty;
                    receiverCompany = _genericAttributeService.GetAttribute<string>(order.Customer, NopCustomerDefaults.CompanyAttribute);
                    var fileBytes = PdfInvoice(orderId);
                    var response = _winstantPayHttpClient.CreateInvoiceAsync(_invoiceSettings.WebServiceUrl, _invoiceSettings.WPayId, receiverWpayId, receiverCompany, loginResponse.Result.Token, fileBytes, order);
                    if (response != null && response.Result != null)
                    {
                        _logger.Information("EventConsumer - CreateInvoice - Invoice is successfully sent to WinstantPay wallet.");
                    }
                    else
                    {
                        _logger.Information("EventConsumer - CreateInvoice - Sending invoice to WinstantPay wallet is failed.");
                    }
                }
                else
                {
                    _logger.Information("WidgetsWinstantPayInvoiceController - CreateInvoice - Login Failed");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("EventConsumer-CreateInvoice-CreateInvoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }
        }


        /// <summary>
        /// Prepare customer attribute models
        /// </summary>
        /// <param name="customer">Customer</param>
        protected virtual string GetCustomerWPayID(Customer customer)
        {
            string wpayID = string.Empty;
            //set already selected attributes
            if (customer != null)
            {
                //get available customer attributes
                var attribute = _customerAttributeService.GetAllCustomerAttributes().Where(ca => ca.Name.Equals("WPayID")).First();
                var attributeModel = new CustomerModel.CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new CustomerModel.CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }

                var selectedCustomerAttributes = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CustomCustomerAttributes);
                if (!string.IsNullOrEmpty(selectedCustomerAttributes))
                {
                    var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
                    if (enteredText.Any())
                    {
                        wpayID = enteredText[0];
                        attributeModel.DefaultValue = enteredText[0];
                    }
                }
            }

            return wpayID;
        }

        private byte[] PdfInvoice(int orderId)
        {
            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }

            var order = _orderService.GetOrderById(orderId);
            var orders = new List<Order>
            {
                order
            };

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _orderSettings.GeneratePdfInvoiceInCustomerLanguage ? 0 : _workContext.WorkingLanguage.Id, vendorId);
                bytes = stream.ToArray();
            }

            return bytes;
        }
    }
}
