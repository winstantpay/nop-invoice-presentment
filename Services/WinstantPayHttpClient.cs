﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Widgets.WinstantPayInvoice.Models;
using Nop.Services.Common;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Models.Orders;
using MediaTypeHeaderValue = Microsoft.Net.Http.Headers.MediaTypeHeaderValue;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Services
{
    public partial class WinstantPayHttpClient
    {
        #region Fields

        private readonly HttpClient _httpClient;
        private readonly ILogger _logger;
        private readonly WinstantPayInvoiceSettings _invoiceSettings;
        private readonly IOrderService _orderService;
        private readonly IPdfService _pdfService;
        private readonly IWorkContext _workContext;
        private readonly OrderSettings _orderSettings;
        private readonly IEncryptionService _encryptionService;
        private string _currencyMultiplier = @"{
                'BTC': 100000000,
                'ETH': 100000000,
                'VND': 1,
                'KRW': 1,
                'JPY': 1,
                'IDR': 1,
              }";

        #endregion

        #region Ctor

        public WinstantPayHttpClient(HttpClient client,
                WinstantPayInvoiceSettings invoiceSettings, 
                ILogger logger,
                IOrderService orderService,
                IPdfService pdfService,
                IWorkContext workContext,
                OrderSettings orderSettings,
                IEncryptionService encryptionService)
        {
            //configure client
            client.Timeout = TimeSpan.FromMilliseconds(5000);
            client.DefaultRequestHeaders.Add(HeaderNames.UserAgent, $"nopCommerce-{NopVersion.CurrentVersion}");

            _httpClient = client;
            _invoiceSettings = invoiceSettings;
            _logger = logger;
            _pdfService = pdfService;
            _workContext = workContext;
            _orderSettings = orderSettings;
            _orderService = orderService;
            _encryptionService = encryptionService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Ping Wallet Service
        /// </summary>
        /// <returns>The asynchronous task whose result contains result of ping</returns>
        public async Task<string> PingWalletServiceAsync()
        {
            var url = _invoiceSettings.WebServiceUrl;
            return await PingWalletServiceAsync(url);
        }

        /// <summary>
        /// Ping Wallet Service
        /// </summary>
        /// <param name="url">URL of web service</param>
        /// <returns>The asynchronous task whose result contains result of ping</returns>
        public async Task<string> PingWalletServiceAsync(string url)
        {
            url = url + "/wallet-service/public/api/v1/ping";
            var response = await _httpClient.GetAsync(url);
            response.EnsureSuccessStatusCode();
            return await response.Content.ReadAsStringAsync();
        }

        /// <summary>
        /// Login to WinstantPay
        /// </summary>
        /// <param name="model">WinstantPayLoginModel</param>
        /// <returns>The asynchronous task whose result contains result of login to WinstantPay</returns>
        public async Task<WinstantPayLoginResultModel> LoginWinstantPayAsync(WinstantPayLoginModel model)
        {
            var url = _invoiceSettings.WebServiceUrl;
            return await LoginWinstantPayAsync(url, model);
        }

        /// <summary>
        /// Login to WinstantPay
        /// </summary>
        /// <param name="url">WinstantPay web service base URL</param>
        /// <param name="model">WinstantPayLoginModel</param>
        /// <returns>The asynchronous task whose result contains result of login to WinstantPay</returns>
        public async Task<WinstantPayLoginResultModel> LoginWinstantPayAsync(string url, WinstantPayLoginModel model)
        {
            var loginResult = new WinstantPayLoginResultModel();
            try
            {
                url = url + "/login";
                var loginJson = JsonConvert.SerializeObject(model);
                var requestContent = new StringContent(loginJson, Encoding.UTF8, "application/json");
                var response = await _httpClient.PostAsync(url, requestContent);

                if (response.IsSuccessStatusCode)
                {
                    response.EnsureSuccessStatusCode();
                    loginResult = JsonConvert.DeserializeObject<WinstantPayLoginResultModel>(await response.Content.ReadAsStringAsync());
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayHttpClient - LoginWinstantPayAsync, Login Error. response: " + JsonConvert.SerializeObject(ex));
            }

            return loginResult;
        }

        /// <summary>
        /// Login to WinstantPay
        /// </summary>
        /// <returns>The asynchronous task whose result contains result of login to WinstantPay</returns>
        public async Task<WinstantPayLoginResultModel> LoginWinstantPayAsync()
        {
            var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
            var model = new WinstantPayLoginModel
            {
                Username = _invoiceSettings.Username,
                Password = decryptedPassword,
                Domain = _invoiceSettings.Domain
            };
            var url = _invoiceSettings.WebServiceUrl;

            return await LoginWinstantPayAsync(url, model);
        }

        /// <summary>
        /// Get invoice detail
        /// </summary>
        /// <param name="url">Base URL of web service</param>
        /// <param name="invoiceId">Invoice Id</param>
        /// <param name="token">Token</param>
        /// <returns>The asynchronous task whose result contains details of an invoice</returns>
        public async Task<InvoiceDetailModel> GetInvoiceDetailAsync(string url, string invoiceId, string token)
        {
            url = url + "/wallet-service/private/api/v1/invoices/" + invoiceId;
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
            var response = await _httpClient.GetAsync(url);
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());

            return result.Data;
        }

        /// <summary>
        /// Get invoice detail
        /// </summary>
        /// <param name="invoiceId">Invoice Id</param>
        /// <param name="token">Token</param>
        /// <returns>The asynchronous task whose result contains details of an invoice</returns>
        public async Task<InvoiceDetailModel> GetInvoiceDetailAsync(string invoiceId, string token)
        {
            var url = _invoiceSettings.WebServiceUrl;
            return await GetInvoiceDetailAsync(url, invoiceId, token);

        }

        /// <summary>
        /// Create new invoice
        /// </summary>
        /// <param name="url">Base URL of web serice</param>
        /// <param name="token">Login token</param>
        /// <param name="model">NewInvoiceModel</param>
        /// <returns>The asynchronous task whose result contains details of new created invoice</returns>
        public async Task<InvoiceDetailModel> CreateInvoiceAsync(string url, string token, NewInvoiceModel model)
        {
            url = url + "/wallet-service/private/api/v1/invoices";
            var newInvoice = JsonConvert.SerializeObject(model);
            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.ToString());
            var requestContent = new StringContent(newInvoice, Encoding.UTF8, "application/json");           
            var response = await _httpClient.PostAsync(url, requestContent);
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());

            return result.Data;
        }

        /// <summary>
        /// Create new invoice
        /// </summary>
        /// <param name="url">Base URL of web serice</param>
        /// <param name="token">Login token</param>
        /// <param name="model">NewInvoiceModel</param>
        /// <returns>The asynchronous task whose result contains details of new created invoice</returns>
        public async Task<InvoiceDetailModel> CreateInvoiceAsync(string url, string token, OrderModel model)
        {
            url = url + "/wallet-service/private/api/v1/invoices";

            var form = new MultipartFormDataContent();

            var response = await _httpClient.PostAsync(url, form);
            response.EnsureSuccessStatusCode();
            var result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());

            return result.Data;
        }

        /// <summary>
        /// Create new invoice
        /// </summary>
        /// <param name="url">Base URL of web serice</param>
        /// <param name="token">Login token</param>
        /// <param name="fileBytes">Bytes array of file</param>
        /// <param name="model">OrderModel</param>
        /// <returns>The asynchronous task whose result contains details of new created invoice</returns>
        public async Task<InvoiceDetailModel> CreateInvoiceAsync(string url, string token, Byte[] fileBytes, OrderModel model)
        {
            url = url + "/wallet-service/private/api/v1/invoices";

            var form = new MultipartFormDataContent();
            var fileContent = new ByteArrayContent(fileBytes);
            fileContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("multipart/form-data");
            fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
            {
                Name = "invoiceFile",
                FileName = $"order_{model.Id}.pdf"
            };
            form.Add(new StringContent(model.CreatedOn.ToString()), "date");
            form.Add(new StringContent(model.Id.ToString()), "invoiceNumber");
            form.Add(new StringContent("Ref: " + model.Id.ToString()), "ref");
            form.Add(new StringContent("THOEUN001 Company"), "receiverCompany");
            form.Add(new StringContent("CHANNTHOEUNKEN Company"), "company");
            form.Add(new StringContent("CHANNTHOEUNKEN"), "wpyId");
            form.Add(new StringContent("THOEUN001"), "receiverWpyId");
            form.Add(new StringContent("channthoeun.ken@gmail.com"), "receiverEmail");
            form.Add(new StringContent(model.OrderTotal), "amount");
            form.Add(new StringContent(DateTime.UtcNow.AddDays(10).ToString()), "dueDate");
            form.Add(new StringContent(model.PrimaryStoreCurrencyCode), "ccy");
            form.Add(fileContent);

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.ToString());
            var response = await _httpClient.PostAsync(url, form);

            var result = new InvoiceDetailResultModel();
            if (response.StatusCode == System.Net.HttpStatusCode.Created)
            {
                response.EnsureSuccessStatusCode();
                result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());
            }

            return result.Data;
        }

        /// <summary>
        /// Create new invoice
        /// </summary>
        /// <param name="url">Base URL of web serice</param>
        /// <param name="token">Login token</param>
        /// <param name="fileBytes">Bytes array of file</param>
        /// <param name="model">Order</param>
        /// <returns>The asynchronous task whose result contains details of new created invoice</returns>
        public async Task<InvoiceDetailModel> CreateInvoiceAsync(string url, string token, Byte[] fileBytes, Order model)
        {
            url = url + "/wallet-service/private/api/v1/invoices";

            var form = new MultipartFormDataContent();
            var fileContent = new ByteArrayContent(fileBytes);
            fileContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("multipart/form-data");
            fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
            {
                Name = "invoiceFile",
                FileName = $"order_{model.Id}.pdf"
            };
            form.Add(new StringContent(model.CreatedOnUtc.ToString()), "date");
            form.Add(new StringContent(model.Id.ToString()), "invoiceNumber");
            form.Add(new StringContent("Ref: " + model.Id.ToString()), "ref");
            form.Add(new StringContent("THOEUN001 Company"), "receiverCompany");
            form.Add(new StringContent("CHANNTHOEUNKEN Company"), "company");
            form.Add(new StringContent("CHANNTHOEUNKEN"), "wpyId");
            form.Add(new StringContent("THOEUN001"), "receiverWpyId");
            form.Add(new StringContent("channthoeun.ken@gmail.com"), "receiverEmail");
            form.Add(new StringContent(Convert.ToInt16(model.OrderTotal).ToString()), "amount");
            form.Add(new StringContent(DateTime.UtcNow.AddDays(10).ToString()), "dueDate");
            form.Add(new StringContent(model.CustomerCurrencyCode), "ccy");
            form.Add(fileContent);

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.ToString());
            var response = await _httpClient.PostAsync(url, form);

            var result = new InvoiceDetailResultModel();
            if (response.StatusCode == System.Net.HttpStatusCode.Created)
            {
                response.EnsureSuccessStatusCode();
                result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());
            }

            return result.Data;
        }

        /// <summary>
        /// Create new invoice
        /// </summary>
        /// <param name="url">Base URL of web serice</param>
        /// <param name="wpayId">WPay Id</param>
        /// <param name="token">Login token</param>
        /// <param name="fileBytes">Bytes array of file</param>
        /// <param name="model">Order</param>
        /// <returns>The asynchronous task whose result contains details of new created invoice</returns>
        public async Task<InvoiceDetailModel> CreateInvoiceAsync(string url, string wpayId, string receiverWpyId, string receiverCompany, string token, Byte[] fileBytes, Order model)
        {
            var data = new InvoiceDetailModel();

            try
            {
                url += "/wallet-service/private/api/v1/invoices";

                var form = new MultipartFormDataContent();
                var fileContent = new ByteArrayContent(fileBytes);
                fileContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("multipart/form-data");
                fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
                {
                    Name = "invoiceFile",
                    FileName = $"order_{model.Id}.pdf"
                };
                form.Add(new StringContent(model.CreatedOnUtc.ToString()), "date");
                form.Add(new StringContent(model.Id.ToString()), "invoiceNumber");
                form.Add(new StringContent($"{DateTime.UtcNow.ToString("yyyy-MM-dd-mm-ss")}-{model.CustomerCurrencyCode}-{wpayId}") , "ref");
                form.Add(new StringContent(receiverCompany), "receiverCompany");
                form.Add(new StringContent($"{wpayId} Company"), "company");
                form.Add(new StringContent(wpayId), "wpyId");
                form.Add(new StringContent(receiverWpyId), "receiverWpyId");
                form.Add(new StringContent(model.Customer.Email), "receiverEmail");
                form.Add(new StringContent(Convert.ToInt32(model.OrderTotal * GetCurrencyMultiplier(model.CustomerCurrencyCode)).ToString()), "amount");
                form.Add(new StringContent(DateTime.UtcNow.AddDays(10).ToString()), "dueDate");
                form.Add(new StringContent(model.CustomerCurrencyCode), "ccy");
                form.Add(fileContent);

                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.ToString());
                var response = await _httpClient.PostAsync(url, form);
                var result = new InvoiceDetailResultModel();

                if (response == null)
                {
                    _logger.Error("WinstantPayHttpClient - CreateInvoiceAsync - CreateInvoice Failed, response is null");
                }
                else if (response.IsSuccessStatusCode)
                {
                    response.EnsureSuccessStatusCode();
                    result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());
                    data = result.Data;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayHttpClient - CreateInvoiceAsync - CreateInvoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return data;
        }

        /// <summary>
        /// Create new invoice
        /// </summary>
        /// <param name="url">Base URL of web serice</param>
        /// <param name="wpayId">WPay Id</param>
        /// <param name="token">Login token</param>
        /// <param name="fileBytes">Bytes array of file</param>
        /// <param name="model">Order</param>
        /// <returns>The asynchronous task whose result contains details of new created invoice</returns>
        public async Task<InvoiceDetailModel> CreateInvoiceAsync(string url, string wpayId, string receiverWpyId, string receiverCompany, string token, Byte[] fileBytes, OrderModel model)
        {
            var data = new InvoiceDetailModel();

            try
            {
                url += "/wallet-service/private/api/v1/invoices";

                var form = new MultipartFormDataContent();
                var fileContent = new ByteArrayContent(fileBytes);
                fileContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("multipart/form-data");
                fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
                {
                    Name = "invoiceFile",
                    FileName = $"order_{model.Id}.pdf"
                };
                form.Add(new StringContent(model.CreatedOn.ToString()), "date");
                form.Add(new StringContent(model.Id.ToString()), "invoiceNumber");
                form.Add(new StringContent($"{DateTime.UtcNow.ToString("yyyy-MM-dd-mm-ss")}-{model.PrimaryStoreCurrencyCode}-{wpayId}"), "ref");
                form.Add(new StringContent(receiverCompany), "receiverCompany");
                form.Add(new StringContent($"{wpayId} Company"), "company");
                form.Add(new StringContent(wpayId), "wpyId");
                form.Add(new StringContent(receiverWpyId), "receiverWpyId");
                form.Add(new StringContent(model.BillingAddress.Email), "receiverEmail");
                form.Add(new StringContent((Convert.ToInt32(model.OrderTotal) * GetCurrencyMultiplier(model.PrimaryStoreCurrencyCode)).ToString()), "amount");
                form.Add(new StringContent(model.CreatedOn.ToString()), "dueDate");
                form.Add(new StringContent(model.PrimaryStoreCurrencyCode), "ccy");
                form.Add(fileContent);

                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await _httpClient.PostAsync(url, form);
                var result = new InvoiceDetailResultModel();

                if (response == null)
                {
                    _logger.Error("WinstantPayHttpClient - CreateInvoiceAsync - CreateInvoice Failed, response is null");
                }
                else if (response.IsSuccessStatusCode)
                {
                    response.EnsureSuccessStatusCode();
                    result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());
                    data = result.Data;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayHttpClient - CreateInvoiceAsync - CreateInvoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return data;
        }

        /// <summary>
        /// Create new invoice
        /// </summary>
        /// <param name="model">NewInvoiceModel</param>
        /// <returns>The asynchronous task whose result contains details of new created invoice</returns>
        public async Task<InvoiceDetailModel> CreateInvoiceAsync(NewInvoiceModel model, string token)
        {
            var data = new InvoiceDetailModel();

            try
            {
                var url = _invoiceSettings.WebServiceUrl;
                url += "/wallet-service/private/api/v1/invoices";

                var form = new MultipartFormDataContent();
                var fileContent = new ByteArrayContent(model.InvoiceFile);
                fileContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("multipart/form-data");
                fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
                {
                    Name = "invoiceFile",
                    FileName = $"order_{model.InvoiceNumber}.pdf"
                };
                form.Add(new StringContent(model.Date.ToString("yyyy-MM-ddTHH:mm:ss")), "date");
                form.Add(new StringContent(model.InvoiceNumber), "invoiceNumber");
                form.Add(new StringContent(model.Company), "company");
                form.Add(new StringContent(model.WpyId), "wpyId");
                form.Add(new StringContent(model.Amount.ToString()), "amount");
                form.Add(new StringContent(model.DueDate.ToString("yyyy-MM-ddTHH:mm:ss")), "dueDate");
                form.Add(new StringContent(model.Ccy), "ccy");
                form.Add(new StringContent(model.StreetAddress), "streetAddress");
                form.Add(new StringContent(model.City), "city");
                form.Add(new StringContent(model.State), "state");
                form.Add(new StringContent(model.Country), "country");
                form.Add(new StringContent(model.PostalCode), "postalCode");
                form.Add(new StringContent(model.Email), "email");
                form.Add(new StringContent(model.Phone), "phone");
                form.Add(new StringContent(model.ReceiverWpyId), "receiverWpyId");
                form.Add(new StringContent(model.ReceiverCompany), "receiverCompany");
                form.Add(new StringContent(model.ReceiverStreetAddress), "receiverStreetAddress");
                form.Add(new StringContent(model.ReceiverCity), "receiverCity");
                form.Add(new StringContent(model.ReceiverState), "receiverState");
                form.Add(new StringContent(model.ReceiverCountry), "receiverCountry");
                form.Add(new StringContent(model.ReceiverPostalCode), "receiverPostalCode");
                form.Add(new StringContent(model.ReceiverEmail), "receiverEmail");
                form.Add(new StringContent(model.ReceiverPhone), "receiverPhone");
                form.Add(fileContent);

                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token);
                var response = await _httpClient.PostAsync(url, form);

                var result = new InvoiceDetailResultModel();

                if (response == null)
                {
                    _logger.Error("WinstantPayHttpClient - CreateInvoiceAsync - CreateInvoice Failed, response is null");
                }
                else if (response.IsSuccessStatusCode)
                {
                    response.EnsureSuccessStatusCode();
                    result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());
                    data = result.Data;
                }
                else
                {
                    _logger.Information("CreateInvoiceAsync - response.StatusCode NOT CREATED, response: " + JsonConvert.SerializeObject(response.Content.ReadAsStringAsync()));
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayHttpClient - CreateInvoiceAsync - CreateInvoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return data;
        }

        /// <summary>
        /// Get invoice detail
        /// </summary>
        /// <param name="url">Base URL of web service</param>
        /// <param name="invoiceId">Invoice Id</param>
        /// <param name="token">Token</param>
        /// <returns>The asynchronous task whose result contains details of an invoice</returns>
        public async Task<InvoiceDetailModel> GetInvoiceByNumberAsync(string url, string invoiceId, string token)
        {
            var invoiceDetail = new InvoiceDetailModel();

            try
            {
                url = url + "/wallet-service/private/api/v1/invoices/invoice-number/" + invoiceId;
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.ToString());
                var response = await _httpClient.GetAsync(url);
                if (response.IsSuccessStatusCode)
                {
                    response.EnsureSuccessStatusCode();
                    var result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());
                    invoiceDetail = result.Data;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("GetInvoiceByNumberAsync - Get invoice by invoice number failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return invoiceDetail;
        }

        /// <summary>
        /// Get invoice detail
        /// </summary>
        /// <param name="invoiceId">Invoice Id</param>
        /// <param name="token">Token</param>
        /// <returns>The asynchronous task whose result contains details of an invoice</returns>
        public async Task<InvoiceDetailModel> GetInvoiceByNumberAsync(string invoiceId, string token)
        {
            var invoiceDetail = new InvoiceDetailModel();

            try
            {
                return await GetInvoiceByNumberAsync(_invoiceSettings.WebServiceUrl, invoiceId, token);
            }
            catch (Exception ex)
            {
                _logger.Error("GetInvoiceByNumberAsync - Get invoice by invoice number failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return invoiceDetail;
        }

        /// <summary>
        /// Create new invoice
        /// </summary>
        /// <param name="url">Base URL of web serice</param>
        /// <param name="wpayId">WPay Id</param>
        /// <param name="token">Login token</param>
        /// <param name="fileBytes">Bytes array of file</param>
        /// <param name="model">Order</param>
        /// <returns>The asynchronous task whose result contains details of new created invoice</returns>
        public async Task<InvoiceDetailModel> CreateMultipleInvoiceAsync(string url, string wpayId, string receiverWpyId, string receiverCompany, string token, Byte[] fileBytes, Order model)
        {
            var data = new InvoiceDetailModel();

            try
            {
                url = url + "/wallet-service/private/api/v1/invoices";
                var form = new MultipartFormDataContent();
                var fileContent = new ByteArrayContent(fileBytes);
                fileContent.Headers.ContentType = System.Net.Http.Headers.MediaTypeHeaderValue.Parse("multipart/form-data");
                fileContent.Headers.ContentDisposition = new System.Net.Http.Headers.ContentDispositionHeaderValue("form-data")
                {
                    Name = "invoiceFile",
                    FileName = $"order_{model.Id}.pdf"
                };
                form.Add(new StringContent(model.CreatedOnUtc.ToString()), "date");
                form.Add(new StringContent(model.Id.ToString()), "invoiceNumber");
                form.Add(new StringContent($"{DateTime.UtcNow.ToString("yyyy-MM-dd-mm-ss")}-{model.CustomerCurrencyCode}-{wpayId}"), "ref");
                form.Add(new StringContent(receiverCompany), "receiverCompany");
                form.Add(new StringContent($"{wpayId} Company"), "company");
                form.Add(new StringContent(wpayId), "wpyId");
                form.Add(new StringContent(receiverWpyId), "receiverWpyId");
                form.Add(new StringContent(model.Customer.Email), "receiverEmail");
                form.Add(new StringContent(Convert.ToInt16(model.OrderTotal * GetCurrencyMultiplier(model.CustomerCurrencyCode)).ToString()), "amount");
                form.Add(new StringContent(DateTime.UtcNow.AddDays(10).ToString()), "dueDate");
                form.Add(new StringContent(model.CustomerCurrencyCode), "ccy");
                form.Add(fileContent);

                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", token.ToString());
                var response = await _httpClient.PostAsync(url, form);
                var result = new InvoiceDetailResultModel();

                if (response.IsSuccessStatusCode)
                {
                    response.EnsureSuccessStatusCode();
                    result = JsonConvert.DeserializeObject<InvoiceDetailResultModel>(await response.Content.ReadAsStringAsync());
                    data = result.Data;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayHttpClient - CreateInvoiceAsync - CreateInvoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return data;
        }

        /// <summary>
        /// Check if invoice number is already existed
        /// </summary>
        /// <param name="url">Base URL of web service</param>
        /// <param name="invoiceId">Invoice Id</param>
        /// <param name="token">Token</param>
        /// <returns>The asynchronous task whose result contains details of an invoice</returns>
        public async Task<bool> IsInvoiceNumberExistsAsync(string url, string invoiceId, string token)
        {
            var isExisted = false;

            try
            {
                var invoiceDetails = await GetInvoiceByNumberAsync(url, invoiceId, token);
                if (invoiceDetails != null && invoiceDetails.Id != null)
                {
                    isExisted = true;
                }    
            }
            catch (Exception ex)
            {
                _logger.Error("IsInvoiceNumberExistsAsync - Get invoice by invoice number failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return isExisted;
        }

        /// <summary>
        /// Check if invoice number is already existed
        /// </summary>
        /// <param name="invoiceId">Invoice Id</param>
        /// <param name="token">Token</param>
        /// <returns>The asynchronous task whose result contains details of an invoice</returns>
        public async Task<bool> IsInvoiceNumberExistsAsync(string invoiceId, string token)
        {
            var isExisted = false;
            var url = _invoiceSettings.WebServiceUrl;

            try
            {
                var invoiceDetails = await GetInvoiceByNumberAsync(url, invoiceId, token);
                if (invoiceDetails != null && invoiceDetails.Id != null)
                {
                    isExisted = true;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("IsInvoiceNumberExistsAsync - Get invoice by invoice number failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return isExisted;
        }

        #endregion

        private byte[] PdfInvoice(int orderId)
        {
            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }

            var order = _orderService.GetOrderById(orderId);
            var orders = new List<Order>
            {
                order
            };

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _orderSettings.GeneratePdfInvoiceInCustomerLanguage ? 0 : _workContext.WorkingLanguage.Id, vendorId);
                bytes = stream.ToArray();
            }

            return bytes;
        }

        private int GetCurrencyMultiplier(string currencyCode)
        {
            int multiplier = 100;
            var multipliers = JObject.Parse(_currencyMultiplier);            

            if (multipliers.ContainsKey(currencyCode))
                multiplier = Convert.ToInt32(multipliers[currencyCode]);

            return multiplier;
        }
    }
}
