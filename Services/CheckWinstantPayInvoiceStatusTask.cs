﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Widgets.WinstantPayInvoice.Models;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Orders;
using Nop.Services.Payments;
using Nop.Services.Security;
using Nop.Services.Tasks;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Services
{
    /// <summary>
    /// Represents a schedule task to renew the access token
    /// </summary>
    public class CheckWinstantPayInvoiceStatusTask : IScheduleTask
    {
        #region Fields

        private readonly ILogger _logger;
        private readonly IOrderService _orderService;
        private readonly WinstantPayInvoiceSettings _invoiceSettings;
        private readonly WinstantPayHttpClient _winstantPayHttpClient;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly WinstantPayInvoiceManager _winstantPayInvoiceManager;
        private readonly IEncryptionService _encryptionService;


        #endregion

        #region Ctor

        public CheckWinstantPayInvoiceStatusTask(
            ILogger logger,
            IOrderService orderService,
            WinstantPayInvoiceSettings invoiceSettings,
            WinstantPayHttpClient winstantPayHttpClient,
            IOrderProcessingService orderProcessingService,
            WinstantPayInvoiceManager winstantPayInvoiceManager,
            IEncryptionService encryptionService)
        {
            _logger = logger;
            _orderService = orderService;
            _invoiceSettings = invoiceSettings;
            _winstantPayHttpClient = winstantPayHttpClient;
            _orderProcessingService = orderProcessingService;
            _winstantPayInvoiceManager = winstantPayInvoiceManager;
            _encryptionService = encryptionService;
        }

        #endregion

        #region Methods

        /// <summary>
        /// Executes a task
        /// </summary>
        public void Execute()
        {
            var ordersStatus = new List<OrderStatusModel>();
            try
            {
                var orders = _orderService.SearchOrders();
                var totalCount = orders.TotalCount;

                ordersStatus = GetInvoicesStatus(orders).Result;
            }
            catch (Exception exception)
            {
                _logger.Error("Execute CheckWinstantPayInvoiceStatusTask Error", exception);
            }
        }

        public async Task<List<OrderStatusModel>> GetInvoicesStatus(IList<Order> orders)
        {
            var invoicesStatus = new List<OrderStatusModel>();

            try
            {
                // Login to WinstantPay
                var loginResult = LoginToWinstantPay();
                if (loginResult != null && loginResult.Token != null)
                {
                    foreach (var order in orders)
                    {
                        if (order.OrderStatus == OrderStatus.Pending)
                        {
                            var orderStatus = new OrderStatusModel();
                            orderStatus.OrderId = order.Id;

                            var response = await _winstantPayInvoiceManager.GetInvoiceByNumber(_winstantPayInvoiceManager.GenerateInvoiceNumber(order), loginResult.Token);

                            if (response != null && response.Status >= 0)
                            {
                                orderStatus.Status = (WinstantPayInvoiceStatus) response.Status;

                                // Check if WinstantPay invoice status is PAID
                                if (orderStatus.Status == WinstantPayInvoiceStatus.Paid)
                                {
                                    // WinstantPay invoice stus is paid, check if the order can be mark as paid
                                    if (_orderProcessingService.CanMarkOrderAsPaid(order))
                                    {
                                        order.AuthorizationTransactionId = response.PaymentId;
                                        _orderService.UpdateOrder(order);
                                        // Mark the invoice as paid
                                        _orderProcessingService.MarkOrderAsPaid(order);
                                    }
                                }
                            }
                            else
                            {
                                // Invoice number not yet existed, create WinstantPay invoice
                                var createInvoiceResponse = await _winstantPayInvoiceManager.CreateInvoiceAsync(order.Id);
                            }

                            invoicesStatus.Add(orderStatus);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("CheckWinstantPayInvoiceStatusTask - GetInvoicesStatus Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return invoicesStatus;

        }

        public OrderStatusModel GetInvoiceStatus(int orderId)
        {
            var orderStatus = new OrderStatusModel();
            try
            {
                // Login to WinstantPay
                var loginResult = LoginToWinstantPay();
                if (loginResult != null && loginResult.Token != null)
                {
                    orderStatus.OrderId = orderId;
                    var response = _winstantPayHttpClient.GetInvoiceByNumberAsync(_invoiceSettings.WebServiceUrl, orderId.ToString(), loginResult.Token);
                    if (response != null && response.Result != null && response.Result.Status >= 0)
                    {
                        orderStatus.Status = (WinstantPayInvoiceStatus) response.Result.Status;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("CheckWinstantPayInvoiceStatusTask - GetInvoiceStatus Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return orderStatus;

        }

        private WinstantPayLoginResultModel LoginToWinstantPay()
        {
            var loginResultModel = new WinstantPayLoginResultModel();
            try
            {
                // Decrypt password
                var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
                var loginModel = new WinstantPayLoginModel
                {
                    Username = _invoiceSettings.Username,
                    Password = decryptedPassword,
                    Domain = _invoiceSettings.Domain
                };

                // Login to WinstantPay
                var loginResponse = _winstantPayHttpClient.LoginWinstantPayAsync(_invoiceSettings.WebServiceUrl, loginModel);
                if (loginResponse != null && loginResponse.Result != null)
                {
                    loginResultModel = loginResponse.Result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("LoginToWinstantPay - Login Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return loginResultModel;
        }

        #endregion
    }
}