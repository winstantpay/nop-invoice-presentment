﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Nop.Core;
using Nop.Core.Domain.Customers;
using Nop.Core.Domain.Directory;
using Nop.Core.Domain.Orders;
using Nop.Plugin.Widgets.WinstantPayInvoice.Models;
using Nop.Services.Common;
using Nop.Services.Customers;
using Nop.Services.Directory;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Factories;
using Nop.Web.Areas.Admin.Models.Customers;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Services
{
    public class WinstantPayInvoiceManager
    {
        #region Fields

        private readonly ILogger _logger;
        private readonly IWorkContext _workContext;
        private readonly IOrderService _orderService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly IPdfService _pdfService;
        private readonly OrderSettings _orderSettings;
        private readonly WinstantPayHttpClient _winstantPayHttpClient;
        private readonly WinstantPayInvoiceSettings _invoiceSettings;
        private readonly CurrencySettings _currencySettings;
        private readonly ICurrencyService _currencyService;
        private readonly IEncryptionService _encryptionService;

        private string _currencyMultiplier = @"{
            'BTC': 100000000,
            'ETH': 100000000,
            'VND': 1,
            'KRW': 1,
            'JPY': 1,
            'IDR': 1,
        }";

        #endregion

        #region Ctor

        public WinstantPayInvoiceManager(ILogger logger,
            IWorkContext workContext,
            IOrderService orderService,
            IGenericAttributeService genericAttributeService,
            ICustomerAttributeService customerAttributeService,
            ICustomerAttributeParser customerAttributeParser,
            IPdfService pdfService,
            OrderSettings orderSettings,
            WinstantPayHttpClient winstantPayHttpClient,
            WinstantPayInvoiceSettings invoiceSettings,
            CurrencySettings currencySettings,
            ICurrencyService currencyService,
            IEncryptionService encryptionService)
        {
            _logger = logger;
            _workContext = workContext;
            _orderService = orderService;
            _genericAttributeService = genericAttributeService;
            _customerAttributeService = customerAttributeService;
            _customerAttributeParser = customerAttributeParser;
            _pdfService = pdfService;
            _orderSettings = orderSettings;
            _winstantPayHttpClient = winstantPayHttpClient;
            _invoiceSettings = invoiceSettings;
            _currencySettings = currencySettings;
            _currencyService = currencyService;
            _encryptionService = encryptionService;
        }

        #endregion

        #region Utilities

        public async Task<WinstantPayLoginResultModel> LoginToWinstantPay()
        {
            var loginResultModel = new WinstantPayLoginResultModel();
            try
            {
                // Decrypting password
                var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
                var loginModel = new WinstantPayLoginModel
                {
                    Username = _invoiceSettings.Username,
                    Password = decryptedPassword,
                    Domain = _invoiceSettings.Domain
                };

                // Login to WinstantPay
                var loginResponse = await LoginToWinstantPay(_invoiceSettings.WebServiceUrl, loginModel);
                if (loginResponse != null && loginResponse.Token != null)
                {
                    loginResultModel = loginResponse;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("LoginToWinstantPay - Login Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return loginResultModel;
        }

        private async Task<WinstantPayLoginResultModel> LoginToWinstantPay(string url, WinstantPayLoginModel model)
        {
            return await _winstantPayHttpClient.LoginWinstantPayAsync(_invoiceSettings.WebServiceUrl, model);
        }

        public async Task<WinstantPayInvoiceStatus> GetInvoiceStatus(int orderId)
        {
            var status = WinstantPayInvoiceStatus.Unknown;
            try
            {
                // Login to WinstantPay
                var loginResult = LoginToWinstantPay();
                if (loginResult != null && loginResult.Result != null && loginResult.Result.Token != null)
                {
                    var token = loginResult.Result.Token;
                    var order = _orderService.GetOrderById(orderId);
                    var response = await _winstantPayHttpClient.GetInvoiceByNumberAsync(_invoiceSettings.WebServiceUrl, orderId.ToString(), token);

                    if (response != null && response.Status >= 0)
                    {
                        status = (WinstantPayInvoiceStatus)response.Status;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayInvoiceManager - GetInvoiceStatus Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return status;
        }

        public async Task<InvoiceDetailModel> CreateInvoiceAsync(int orderId)
        {
            var invoiceDetailModel = new InvoiceDetailModel();
            try
            {
                // Login to WinstantPay
                var loginResponse = await LoginToWinstantPay();

                if (loginResponse != null)
                {
                    // Create WinstantPay invoice
                    var response = await CreateInvoiceAsync(orderId, loginResponse.Token);

                    if (response != null)
                    {
                        invoiceDetailModel = response;
                    }
                }
                else
                {
                    _logger.Information("WinstantPayInvoiceManager - CreateInvoice - Login Failed");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayInvoiceManager-CreateInvoice-CreateInvoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return invoiceDetailModel;
        }

        public async Task<InvoiceDetailModel> CreateInvoiceAsync(int orderId, string token)
        {
            var invoiceDetailModel = new InvoiceDetailModel();
            try
            {
                // Get order
                var order = _orderService.GetOrderById(orderId);
                // Prepare invoice model
                var model = PrepareNewInvoiceModel(order);
                // Create WinstantPay invoice
                var response = await _winstantPayHttpClient.CreateInvoiceAsync(model, token);

                if (response != null)
                {
                    invoiceDetailModel = response;
                }
                else
                {
                    // Creating WinstantPay invoice failed.
                    _logger.Error("WinstantPayInvoiceManager-CreateInvoiceAsync-Sending invoice to WinstantPay wallet is failed.");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayInvoiceManager-CreateInvoiceAsync-Create Invoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return invoiceDetailModel;
        }

        public InvoiceDetailModel CreateInvoice(int orderId)
        {
            var invoiceDetailModel = new InvoiceDetailModel();
            try
            {
                var response =  CreateInvoiceAsync(orderId);

                if (response != null)
                {
                    invoiceDetailModel = response.Result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayInvoiceManager-CreateInvoice-CreateInvoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return invoiceDetailModel;
        }

        public async Task<InvoiceDetailModel> GetInvoiceDetail(string invoiceNumbeer, string token)
        {
            var model = new InvoiceDetailModel();
            try
            {
                // Create WinstantPay invoice
                var response = await _winstantPayHttpClient.GetInvoiceDetailAsync(invoiceNumbeer, token);
                if (response != null)
                {
                    model = response;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayInvoiceManager-GetInvoiceDetail error, ex: " + JsonConvert.SerializeObject(ex));
            }

            return model;
        }

        public async Task<InvoiceDetailModel> GetInvoiceByNumber(string invoiceNumbeer, string token)
        {
            var model = new InvoiceDetailModel();
            try
            {
                var response = await _winstantPayHttpClient.GetInvoiceByNumberAsync(invoiceNumbeer, token);
                if (response != null)
                {
                    model = response;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayInvoiceManager-GetInvoiceByNumber error, ex: " + JsonConvert.SerializeObject(ex));
            }

            return model;
        }

        public async Task<string> GetInvoicePublicUrl(string invoiceNumbeer, string token)
        {
            var publicUrl = string.Empty;

            try
            {
                var response = await _winstantPayHttpClient.GetInvoiceByNumberAsync(invoiceNumbeer, token);
                if (response != null)
                {
                    publicUrl += "http://" + _invoiceSettings.Domain + "/invoice/" + response.Id;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayInvoiceManager-GetInvoiceByNumber error, ex: " + JsonConvert.SerializeObject(ex));
            }

            return publicUrl;
        }
        public async Task<bool> IsInvoiceNumberExistedAsync(string invoiceNumbeer, string token)
        {
            bool isExisted = true;
            try
            {
                isExisted = await _winstantPayHttpClient.IsInvoiceNumberExistsAsync(invoiceNumbeer, token);                
            }
            catch (Exception ex)
            {
                _logger.Error("WinstantPayInvoiceManager-IsInvoiceNumberExistedAsync error, ex: " + JsonConvert.SerializeObject(ex));
            }

            return isExisted;
        }

        public bool IsInvoiceNumberExisted(string invoiceNumbeer, string token)
        {
            return IsInvoiceNumberExistedAsync(invoiceNumbeer, token).Result;
        }

        /// <summary>
        /// Prepare customer attribute models
        /// </summary>
        /// <param name="customer">Customer</param>
        public string GetCustomerWPayID(Customer customer)
        {
            string wpayID = string.Empty;
            //set already selected attributes
            if (customer != null)
            {
                //get available customer attributes
                var allAttributes = _customerAttributeService.GetAllCustomerAttributes();
                if (allAttributes != null && allAttributes.Count > 0)
                {
                    var attribute = allAttributes.Where(ca => ca.Name.Equals("WPayID")).First();
                    if (attribute != null)
                    {
                        var attributeModel = new CustomerModel.CustomerAttributeModel
                        {
                            Id = attribute.Id,
                            Name = attribute.Name,
                            IsRequired = attribute.IsRequired,
                            AttributeControlType = attribute.AttributeControlType
                        };

                        if (attribute.ShouldHaveValues())
                        {
                            //values
                            var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                            foreach (var attributeValue in attributeValues)
                            {
                                var attributeValueModel = new CustomerModel.CustomerAttributeValueModel
                                {
                                    Id = attributeValue.Id,
                                    Name = attributeValue.Name,
                                    IsPreSelected = attributeValue.IsPreSelected
                                };
                                attributeModel.Values.Add(attributeValueModel);
                            }
                        }

                        var selectedCustomerAttributes = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CustomCustomerAttributes);
                        if (!string.IsNullOrEmpty(selectedCustomerAttributes))
                        {
                            var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
                            if (enteredText.Any())
                            {
                                wpayID = enteredText[0];
                                attributeModel.DefaultValue = enteredText[0];
                            }


                        }
                    }
                }
            }

            return wpayID;
        }

        public byte[] PdfInvoice(int orderId)
        {
            //a vendor should have access only to his products
            var vendorId = 0;
            if (_workContext.CurrentVendor != null)
            {
                vendorId = _workContext.CurrentVendor.Id;
            }

            var order = _orderService.GetOrderById(orderId);
            var orders = new List<Order>
            {
                order
            };

            byte[] bytes;
            using (var stream = new MemoryStream())
            {
                _pdfService.PrintOrdersToPdf(stream, orders, _orderSettings.GeneratePdfInvoiceInCustomerLanguage ? 0 : _workContext.WorkingLanguage.Id, vendorId);
                bytes = stream.ToArray();
            }

            return bytes;
        }

        public NewInvoiceModel PrepareNewInvoiceModel(Order order)
        {
            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            var wpayId = _invoiceSettings.WPayId;
            var receiverCompany = _genericAttributeService.GetAttribute<string>(order.Customer, NopCustomerDefaults.CompanyAttribute) != null ? _genericAttributeService.GetAttribute<string>(order.Customer, NopCustomerDefaults.CompanyAttribute) : string.Empty;
            var receiverEmail = order.Customer.IsRegistered() ? order.Customer.Email : order.BillingAddress.Email ;
            var receiverWpyId = order.Customer.IsRegistered() ? GetCustomerWPayID(order.Customer) : String.Empty;
            var company = _invoiceSettings.Company;

            return  new NewInvoiceModel
            {
                Amount = Convert.ToInt32(order.OrderTotal * GetCurrencyMultiplier(order.CustomerCurrencyCode)),
                Ccy = primaryStoreCurrency.CurrencyCode,
                Company = company,
                Date = order.CreatedOnUtc,
                DueDate = order.CreatedOnUtc,
                InvoiceFile = PdfInvoice(order.Id),
                InvoiceNumber = GenerateInvoiceNumber(order),
                ReceiverCompany = receiverCompany,
                ReceiverEmail = receiverEmail,
                ReceiverWpyId = receiverWpyId,
                WpyId = wpayId,
                StreetAddress = _invoiceSettings.StreetAddress,
                City = _invoiceSettings.City,
                State = _invoiceSettings.State,
                Country = _invoiceSettings.Country,
                PostalCode = _invoiceSettings.PostalCode,
                Email = _invoiceSettings.Email,
                Phone = _invoiceSettings.Phone,
                ReceiverStreetAddress = order.BillingAddress.Address1 + (string.IsNullOrEmpty(order.BillingAddress.Address2)? string.Empty : ", " + order.BillingAddress.Address2),
                ReceiverPostalCode = string.IsNullOrEmpty(order.BillingAddress.ZipPostalCode)? string.Empty : order.BillingAddress.ZipPostalCode,
                ReceiverCity = string.IsNullOrEmpty(order.BillingAddress.City)? string.Empty : order.BillingAddress.City,
                ReceiverCountry = (order.BillingAddress.Country != null)? order.BillingAddress.Country?.TwoLetterIsoCode : String.Empty,
                ReceiverState = (order.BillingAddress.StateProvince != null)? order.BillingAddress.StateProvince?.Name : String.Empty,
                ReceiverPhone = string.IsNullOrEmpty(order.BillingAddress.PhoneNumber)? string.Empty : order.BillingAddress.PhoneNumber,
            };
        }

        private int GetCurrencyMultiplier(string currencyCode)
        {
            int multiplier = 100;
            var multipliers = JObject.Parse(_currencyMultiplier);

            if (multipliers.ContainsKey(currencyCode))
                multiplier = Convert.ToInt32(multipliers[currencyCode]);

            return multiplier;
        }

        public string GenerateInvoiceNumber(Order order)
        {
            var primaryStoreCurrency = _currencyService.GetCurrencyById(_currencySettings.PrimaryStoreCurrencyId);
            var wpayId = _invoiceSettings.WPayId;

            return $"{order.CreatedOnUtc.ToString("yyyyMMddmmss")}-{primaryStoreCurrency.CurrencyCode}-{wpayId}-{order.Id}";

        }

        public string GenerateInvoiceNumber(int orderId)
        {
            var order = _orderService.GetOrderById(orderId);
            return GenerateInvoiceNumber(order);
        }

        #endregion
    }
}
