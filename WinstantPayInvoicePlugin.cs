﻿using System.Collections.Generic;
using Nop.Core;
using Nop.Core.Domain.Cms;
using Nop.Core.Domain.Tasks;
using Nop.Core.Infrastructure;
using Nop.Services.Cms;
using Nop.Services.Configuration;
using Nop.Services.Localization;
using Nop.Services.Media;
using Nop.Services.Plugins;
using Nop.Services.Security;
using Nop.Services.Tasks;
using Nop.Web.Framework.Infrastructure;

namespace Nop.Plugin.Widgets.WinstantPayInvoice
{
    /// <summary>
    /// PLugin
    /// </summary>
    public class WinstantPayInvoicePlugin : BasePlugin, IWidgetPlugin
    {
        private readonly ILocalizationService _localizationService;
        private readonly ISettingService _settingService;
        private readonly IWebHelper _webHelper;
        private readonly IScheduleTaskService _scheduleTaskService;
        private readonly IEncryptionService _encryptionService;

        public WinstantPayInvoicePlugin(ILocalizationService localizationService,
            ISettingService settingService,
            IWebHelper webHelper,
            IScheduleTaskService scheduleTaskService,
            IEncryptionService encryptionService)
        {
            _localizationService = localizationService;
            _settingService = settingService;
            _webHelper = webHelper;
            _scheduleTaskService = scheduleTaskService;
            _encryptionService = encryptionService;
        }

        /// <summary>
        /// Gets widget zones where this widget should be rendered
        /// </summary>
        /// <returns>Widget zones</returns>
        public IList<string> GetWidgetZones()
        {
            return new List<string> { AdminWidgetZones.OrderDetailsButtons, AdminWidgetZones.OrderListButtons, PublicWidgetZones.OrderDetailsPageOverview };
        }

        //public void GetDisplayWidgetRoute(string widgetZone, out string actionName, out string controllerName, out RouteValueDictionary routeValues)
        //{

        //}
        /// <summary>
        /// Gets a configuration page URL
        /// </summary>
        public override string GetConfigurationPageUrl()
        {
            return _webHelper.GetStoreLocation() + "Admin/WidgetsWinstantPayInvoice/Configure";
        }

        /// <summary>
        /// Gets a name of a view component for displaying widget
        /// </summary>
        /// <param name="widgetZone">Name of the widget zone</param>
        /// <returns>View component name</returns>
        public string GetWidgetViewComponentName(string widgetZone)
        {
            return "WidgetsWinstantPayInvoice";
        }

        /// <summary>
        /// Install plugin
        /// </summary>
        public override void Install()
        {
            //settings
            var settings = new WinstantPayInvoiceSettings
            {
                Username = "Channthoeun Customer User",
                Password = _encryptionService.EncryptText("Thoeun051182"),
                WPayId = "CHANNTHOEUNKEN",
                Domain = "localhost:4200",
                WebServiceUrl = "http://localhost:8100",
                IsShowViewInvoiceButtonOnCustomerOrderDetailPage = true,
                IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage = true,
                IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage = true,
                IsSendInvoiceToWinstantPayWhenOrderPlaced = true,
                Company = "WinstantPay",
                StreetAddress = "No. 123, street 456",
                City = "Phnom Penh",
                State = "",
                Country = "KH",
                PostalCode = "",
                Email = "channthoeun@gmail.com",
                Phone = "+855 12 274 140"
            };

            _settingService.SaveSetting(settings);

            //install synchronization task
            if (_scheduleTaskService.GetTaskByType(WinstantPayInvoiceDefaults.CheckWinstantPayInvoiceStatusTask) == null)
            {
                _scheduleTaskService.InsertTask(new ScheduleTask
                {
                    Enabled = true,
                    Seconds = WinstantPayInvoiceDefaults.DefaultCheckWinstantPayInvoiceStatusPeriod,
                    Name = WinstantPayInvoiceDefaults.CheckWinstantPayInvoiceStatusTaskName,
                    Type = WinstantPayInvoiceDefaults.CheckWinstantPayInvoiceStatusTask,
                });
            }

            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Username", "Username");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Username.Hint", "WinstantPay username.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Password", "Password");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Password.Hint", "WinstantPay user password.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.WPayId", "WPay ID");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.WPayId.Hint", "WinstantPay WPay ID.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Domain", "Domain");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Domain.Hint", "WinstantPay user domain.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.WebServiceUrl", "Web Service URL");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.WebServiceUrl.Hint", "WinstantPay web service URL.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.SendButton.Text", "Send To WinstantPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.SendButton.Text.Hint", "Send invoice details to WinstantPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.GetStatusButton.Text", "Get Invoice Status");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.GetStatusButton.Text.Hint", "Get invoice status from WinstantPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.SendMultipleInvoicesButton.Text", "Create Invoices");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.SendMultipleInvoicesButton.Text.Hint", "Create multiple invoices to WinstantPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.ViewInvoice.Text", "View Invoice on WinstantPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.ViewInvoice.Text.Hint", "View Invoice on WinstantPay");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowViewInvoiceButtonOnCustomerOrderDetailPage", "Show View Invoice on WinstantPay on Customer Order Detail Page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowViewInvoiceButtonOnCustomerOrderDetailPage.Hint", "Show View Invoice on WinstantPay on Customer Order Detail Page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage", "Show Get Invoice Status Button on Admin Order Detail Page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage.Hint", "Show Get Invoice Status Button on Admin Order Detail Page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage", "Show Send Invoice to WinstantPay Button on Admin Order Detail Page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage.Hint", "Show Send Invoice to WinstantPay button on Admin order details page");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsSendInvoiceToWinstantPayWhenOrderPlaced", "Send Invoice to WinstantPay When Order Placed");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsSendInvoiceToWinstantPayWhenOrderPlaced.Hint", "Send Invoice to WinstantPay When Order Placed");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Company", "Company");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Company.Hint", "Company of WinstantPay Invoice Presenter.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.StreetAddress", "Street Address");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.StreetAddress.Hint", "Street Address of WinstantPay Invoice Presenter.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.City", "City");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.City.Hint", "City of WinstantPay Invoice Presenter.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.State", "State");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.State.Hint", "State of WinstantPay Invoice Presenter.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Country", "Country");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Country.Hint", "Country of WinstantPay Invoice Presenter.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.PostalCode", "Postal Code");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.PostalCode.Hint", "Postal Code of WinstantPay Invoice Presenter.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Email", "Email");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Email.Hint", "Email of WinstantPay Invoice Presenter.");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Phone", "Phone");
            _localizationService.AddOrUpdatePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Phone.Hint", "Phone of WinstantPay Invoice Presenter.");


            base.Install();
        }

        /// <summary>
        /// Uninstall plugin
        /// </summary>
        public override void Uninstall()
        {
            //settings
            _settingService.DeleteSetting<WinstantPayInvoiceSettings>();

            //remove scheduled task
            var task = _scheduleTaskService.GetTaskByType(WinstantPayInvoiceDefaults.CheckWinstantPayInvoiceStatusTask);
            if (task != null)
                _scheduleTaskService.DeleteTask(task);

            //locales
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Username");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Username.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Password");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Password.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.WPayId");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.WPayId.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Domain");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Domain.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.WebServiceUrl");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.WebServiceUrl.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.SendButton.Text");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.SendButton.Text.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.GetStatusButton.Text");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.GetStatusButton.Text.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.SendMultipleInvoicesButton.Text");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.SendMultipleInvoicesButton.Text.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.ViewInvoice.Text");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.ViewInvoice.Text.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowViewInvoiceButtonOnCustomerOrderDetailPage");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowViewInvoiceButtonOnCustomerOrderDetailPage.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsSendInvoiceToWinstantPayWhenOrderPlaced");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.IsSendInvoiceToWinstantPayWhenOrderPlaced.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Company");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Company.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.StreetAddress");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.StreetAddress.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.City");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.City.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.State");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.State.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Country");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Country.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.PostalCode");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.PostalCode.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Email");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Email.Hint");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Phone");
            _localizationService.DeletePluginLocaleResource("Plugins.Widgets.WinstantPayInvoice.Phone.Hint");

            base.Uninstall();
        }

        /// <summary>
        /// Gets a value indicating whether to hide this plugin on the widget list page in the admin area
        /// </summary>
        public bool HideInWidgetList => false;
    }
}