﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Nop.Core;
using Nop.Core.Domain.Catalog;
using Nop.Core.Domain.Customers;
using Nop.Plugin.Widgets.WinstantPayInvoice.Models;
using Nop.Plugin.Widgets.WinstantPayInvoice.Services;
using Nop.Services.Common;
using Nop.Services.Configuration;
using Nop.Services.Customers;
using Nop.Services.Localization;
using Nop.Services.Logging;
using Nop.Services.Messages;
using Nop.Services.Orders;
using Nop.Services.Security;
using Nop.Web.Areas.Admin.Models.Customers;
using Nop.Web.Framework;
using Nop.Web.Framework.Controllers;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Controllers
{
    [Area(AreaNames.Admin)]
    public class WidgetsWinstantPayInvoiceController : BasePluginController
    {
        private readonly ILocalizationService _localizationService;
        private readonly INotificationService _notificationService;
        private readonly IPermissionService _permissionService;
        private readonly ISettingService _settingService;
        private readonly IStoreContext _storeContext;
        private readonly IOrderProcessingService _orderProcessingService;
        private readonly IOrderService _orderService;
        private readonly WinstantPayInvoiceSettings _invoiceSettings;
        private readonly WinstantPayHttpClient _winstantPayHttpClient;
        private readonly ILogger _logger;
        private readonly ICustomerAttributeService _customerAttributeService;
        private readonly IGenericAttributeService _genericAttributeService;
        private readonly ICustomerAttributeParser _customerAttributeParser;
        private readonly WinstantPayInvoiceManager _winstantPayInvoiceManager;
        private readonly IEncryptionService _encryptionService;

        public WidgetsWinstantPayInvoiceController(ILocalizationService localizationService,
            INotificationService notificationService,
            IPermissionService permissionService,
            ISettingService settingService,
            IStoreContext storeContext,
            IOrderProcessingService orderProcessingService,
            IOrderService orderService,
            ILogger logger,
            WinstantPayInvoiceSettings invoiceSettings,
            WinstantPayHttpClient winstantPayHttpClient,
            ICustomerAttributeService customerAttributeService,
            IGenericAttributeService genericAttributeService,
            ICustomerAttributeParser customerAttributeParser,
            WinstantPayInvoiceManager winstantPayInvoiceManager,
            IEncryptionService encryptionService)
        {
            _localizationService = localizationService;
            _notificationService = notificationService;
            _permissionService = permissionService;
            _settingService = settingService;
            _storeContext = storeContext;
            _orderProcessingService = orderProcessingService;
            _orderService = orderService;
            _invoiceSettings = invoiceSettings;
            _winstantPayHttpClient = winstantPayHttpClient;
            _logger = logger;
            _customerAttributeService = customerAttributeService;
            _genericAttributeService = genericAttributeService;
            _customerAttributeParser = customerAttributeParser;
            _winstantPayInvoiceManager = winstantPayInvoiceManager;
            _encryptionService = encryptionService;
        }

        public IActionResult Configure()
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var invoiceSettings = _settingService.LoadSetting<WinstantPayInvoiceSettings>(storeScope);
            var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
            
            var model = new ConfigurationModel
            {
                Username = invoiceSettings.Username,
                Password = decryptedPassword,
                WPayId = invoiceSettings.WPayId,
                Domain = invoiceSettings.Domain,
                WebServiceUrl = invoiceSettings.WebServiceUrl,
                ActiveStoreScopeConfiguration = storeScope,
                IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage = invoiceSettings.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage,
                IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage = invoiceSettings.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage,
                IsShowViewInvoiceButtonOnCustomerOrderDetailPage = invoiceSettings.IsShowViewInvoiceButtonOnCustomerOrderDetailPage,
                IsSendInvoiceToWinstantPayWhenOrderPlaced = invoiceSettings.IsSendInvoiceToWinstantPayWhenOrderPlaced,
                Company = invoiceSettings.Company,
                StreetAddress = invoiceSettings.StreetAddress,
                City = invoiceSettings.City,
                State = invoiceSettings.State,
                Country = invoiceSettings.Country,
                PostalCode = invoiceSettings.PostalCode,
                Email = invoiceSettings.Email,
                Phone = invoiceSettings.Phone
            };

            if (storeScope > 0)
            {
                model.Username_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.Username, storeScope);
                model.Password_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.Password, storeScope);
                model.WPayId_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.WPayId, storeScope);
                model.Domain_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.Domain, storeScope);
                model.WebServiceUrl_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.WebServiceUrl, storeScope);
                model.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage, storeScope);
                model.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage, storeScope);
                model.IsShowViewInvoiceButtonOnCustomerOrderDetailPage_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.IsShowViewInvoiceButtonOnCustomerOrderDetailPage, storeScope);
                model.IsSendInvoiceToWinstantPayWhenOrderPlaced_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.IsSendInvoiceToWinstantPayWhenOrderPlaced, storeScope);
                model.Company_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.Company, storeScope);
                model.StreetAddress_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.StreetAddress, storeScope);
                model.City_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.City, storeScope);
                model.State_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.State, storeScope);
                model.Country_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.Country, storeScope);
                model.PostalCode_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.PostalCode, storeScope);
                model.Email_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.Email, storeScope);
                model.Phone_OverrideForStore = _settingService.SettingExists(invoiceSettings, x => x.Phone, storeScope);
            }

            return View("~/Plugins/Widgets.WinstantPayInvoice/Views/Configure.cshtml", model);
        }

        [HttpPost]
        public IActionResult Configure(ConfigurationModel model)
        {
            if (!_permissionService.Authorize(StandardPermissionProvider.ManageWidgets))
                return AccessDeniedView();

            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var invoiceSettings = _settingService.LoadSetting<WinstantPayInvoiceSettings>(storeScope);

            invoiceSettings.Username = model.Username;
            invoiceSettings.WPayId = model.WPayId;
            invoiceSettings.Domain = model.Domain;
            invoiceSettings.WebServiceUrl = model.WebServiceUrl;
            invoiceSettings.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage = model.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage;
            invoiceSettings.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage = model.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage;
            invoiceSettings.IsShowViewInvoiceButtonOnCustomerOrderDetailPage = model.IsShowViewInvoiceButtonOnCustomerOrderDetailPage;
            invoiceSettings.IsSendInvoiceToWinstantPayWhenOrderPlaced = model.IsSendInvoiceToWinstantPayWhenOrderPlaced;
            invoiceSettings.Company = model.Company;
            invoiceSettings.StreetAddress = model.StreetAddress;
            invoiceSettings.City = model.City;
            invoiceSettings.State = model.State;
            invoiceSettings.Country = model.Country;
            invoiceSettings.PostalCode = model.PostalCode;
            invoiceSettings.Email = model.Email;
            invoiceSettings.Phone = model.Phone;

            /* We do not clear cache after each setting update.
             * This behavior can increase performance because cached settings will not be cleared 
             * and loaded from database after each update */
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.Username, model.Username_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.Password, model.Password_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.WPayId, model.WPayId_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.Domain, model.Domain_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.WebServiceUrl, model.WebServiceUrl_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage, model.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage, model.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.IsShowViewInvoiceButtonOnCustomerOrderDetailPage, model.IsShowViewInvoiceButtonOnCustomerOrderDetailPage_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.IsSendInvoiceToWinstantPayWhenOrderPlaced, model.IsSendInvoiceToWinstantPayWhenOrderPlaced_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.Company, model.Company_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.StreetAddress, model.StreetAddress_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.City, model.City_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.State, model.State_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.Country, model.Country_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.PostalCode, model.PostalCode_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.Email, model.Email_OverrideForStore, storeScope, false);
            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.Phone, model.Phone_OverrideForStore, storeScope, false);

            //now clear settings cache
            _settingService.ClearCache();

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }

        [HttpPost, ActionName("Configure")]
        [FormValueRequired("changepassword")]
        public IActionResult ChangePassword(ConfigurationModel model)
        {
            //load settings for a chosen store scope
            var storeScope = _storeContext.ActiveStoreScopeConfiguration;
            var invoiceSettings = _settingService.LoadSetting<WinstantPayInvoiceSettings>(storeScope);

            invoiceSettings.Password = _encryptionService.EncryptText(model.Password);

            _settingService.SaveSettingOverridablePerStore(invoiceSettings, x => x.Password, model.Password_OverrideForStore, storeScope, false);

            // now clear settings cache
            _settingService.ClearCache();

            _notificationService.SuccessNotification(_localizationService.GetResource("Admin.Plugins.Saved"));
            return Configure();
        }

        public IActionResult PingWinstantPayService(int orderId)
        {
            var response = _winstantPayHttpClient.PingWalletServiceAsync(_invoiceSettings.WebServiceUrl);

            return RedirectToRoute("Admin.Order.Edit", new { id = orderId });
        }

        public IActionResult GetInvoiceDetail(int orderId)
        {
            string invoiceId = "38419010-7371-4d39-94b9-28c8cebabcde";

            // Dcrypt password
            var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
            var loginModel = new WinstantPayLoginModel
            {
                Username = _invoiceSettings.Username,
                Password = decryptedPassword,
                Domain = _invoiceSettings.Domain
            };

            // Login to WinstantPay
            var loginResponse = _winstantPayHttpClient.LoginWinstantPayAsync(_invoiceSettings.WebServiceUrl, loginModel);
            if (loginResponse != null && loginResponse.Result != null)
            {
                var response = _winstantPayHttpClient.GetInvoiceDetailAsync(_invoiceSettings.WebServiceUrl, invoiceId, loginResponse.Result.Token);
            }

            return RedirectToRoute("Admin.Order.Edit", new { id = orderId });
        }

        public IActionResult CreateInvoice(int orderId)
        {
            try
            {
                // Create WinstantPay invoice
                var response = _winstantPayInvoiceManager.CreateInvoiceAsync(orderId);
                if (response != null && response.Result != null)
                {
                    // Create WinstantPay invoice success, show success notification
                    _notificationService.SuccessNotification("Invoice is successfully sent to WinstantPay wallet.");
                }
                else
                {
                    // Create WinstantPay invoice failed, show error notification
                    _notificationService.ErrorNotification("Sendingg invoice to WinstantPay wallet is failed.");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WidgetsInvoiceController-CreateInvoice-CreateInvoice Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return RedirectToRoute("Admin.Order.Edit", new { id = orderId });
        }

        public IActionResult GetInvoiceByNumber(int orderId)
        {
            try
            {
                // Login to WinstantPay
                var loginResult = LoginToWinstantPay();
                if (loginResult != null && loginResult.Token != null)
                {
                    var response = _winstantPayHttpClient.GetInvoiceByNumberAsync(_invoiceSettings.WebServiceUrl, orderId.ToString(), loginResult.Token);
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WidgetsInvoiceController - GetInvoiceByNumber Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return RedirectToRoute("Admin.Order.Edit", new { id = orderId });
        }

        public async Task<IActionResult> GetInvoiceStatus(int orderId)
        {
            try
            {
                // Login to WinstantPay
                var loginResult = await _winstantPayInvoiceManager.LoginToWinstantPay();

                if (loginResult != null && loginResult.Token != null)
                {
                    var response = await _winstantPayInvoiceManager.GetInvoiceByNumber(_winstantPayInvoiceManager.GenerateInvoiceNumber(orderId), loginResult.Token);

                    if (response != null && response.Status >= 0)
                    {
                        // Check if WinstantPay invoice status is PAID
                        if (((WinstantPayInvoiceStatus)response.Status) == WinstantPayInvoiceStatus.Paid)
                        {
                            // WinstantPay invoice status is PAID
                            var order = _orderService.GetOrderById(orderId);
                            if (_orderProcessingService.CanMarkOrderAsPaid(order))
                            {
                                order.AuthorizationTransactionId = response.PaymentId;
                                _orderService.UpdateOrder(order);

                                // Mark order as paid
                                _orderProcessingService.MarkOrderAsPaid(order);
                            }
                        }
                    }
                    _notificationService.SuccessNotification("Invoice Status: " + ((WinstantPayInvoiceStatus)response.Status).ToString());
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WidgetsInvoiceController - GetInvoiceStatus Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return RedirectToRoute("Admin.Order.Edit", new { id = orderId });
        }

        private WinstantPayLoginResultModel LoginToWinstantPay()
        {
            var loginResultModel = new WinstantPayLoginResultModel();
            try
            {
                // Decrypt password
                var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
                var loginModel = new WinstantPayLoginModel
                {
                    Username = _invoiceSettings.Username,
                    Password = decryptedPassword,
                    Domain = _invoiceSettings.Domain
                };

                // Login to WinstantPay
                var loginResponse = _winstantPayHttpClient.LoginWinstantPayAsync(_invoiceSettings.WebServiceUrl, loginModel);
                if (loginResponse != null && loginResponse.Result != null)
                {
                    loginResultModel = loginResponse.Result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("LoginToWinstantPay - Login Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return loginResultModel;
        }

        private bool IsInvoiceNumberAvailable(int orderId)
        {
            var isAvailable = false;

            try
            {
                _logger.Information("WidgetsWinstantPayInvoiceController - IsInvoiceNumberAvailable start, orderId: " + orderId);

                // Login to WinstantPay
                var loginResult = LoginToWinstantPay();
                if (loginResult != null && loginResult.Token != null)
                {
                    _logger.Information("IsInvoiceNumberAvailable - After Login Successful");

                    _logger.Information("IsInvoiceNumberAvailable - Start getting invoice detail by number");
                    var response = _winstantPayHttpClient.IsInvoiceNumberExistsAsync(_invoiceSettings.WebServiceUrl, orderId.ToString(), loginResult.Token);
                    _logger.Information("WidgetsWinstantPayInvoiceController - IsInvoiceNumberAvailable, response: " + JsonConvert.SerializeObject(response.Result));
                    isAvailable = !response.Result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WidgetsWinstantPayInvoiceController - GetInvoiceByNumber Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return isAvailable;
        }

        #region Utilities

        private Task<WinstantPayLoginResultModel> LoginToWinstantPay(string url, WinstantPayLoginModel model)
        {
             var response = _winstantPayHttpClient.LoginWinstantPayAsync(_invoiceSettings.WebServiceUrl, model);
            return response;
        }

        /// <summary>
        /// Prepare customer attribute models
        /// </summary>
        /// <param name="customer">Customer</param>
        protected virtual void PrepareCustomerAttributeModels(Customer customer)
        {
            // get available customer attributes
            var customerAttributes = _customerAttributeService.GetAllCustomerAttributes().Where( ca => ca.Name.Equals("WPayID"));

            foreach (var attribute in customerAttributes)
            {
                var attributeModel = new CustomerModel.CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new CustomerModel.CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }

                //set already selected attributes
                if (customer != null)
                {
                    var selectedCustomerAttributes = _genericAttributeService
                        .GetAttribute<string>(customer, NopCustomerDefaults.CustomCustomerAttributes);
                    switch (attribute.AttributeControlType)
                    {
                        case AttributeControlType.DropdownList:
                        case AttributeControlType.RadioList:
                        case AttributeControlType.Checkboxes:
                            {
                                if (!string.IsNullOrEmpty(selectedCustomerAttributes))
                                {
                                    //clear default selection
                                    foreach (var item in attributeModel.Values)
                                        item.IsPreSelected = false;

                                    //select new values
                                    var selectedValues = _customerAttributeParser.ParseCustomerAttributeValues(selectedCustomerAttributes);
                                    foreach (var attributeValue in selectedValues)
                                        foreach (var item in attributeModel.Values)
                                            if (attributeValue.Id == item.Id)
                                                item.IsPreSelected = true;
                                }
                            }
                            break;
                        case AttributeControlType.ReadonlyCheckboxes:
                            {
                                //do nothing
                                //values are already pre-set
                            }
                            break;
                        case AttributeControlType.TextBox:
                        case AttributeControlType.MultilineTextbox:
                            {
                                if (!string.IsNullOrEmpty(selectedCustomerAttributes))
                                {
                                    var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
                                    if (enteredText.Any())
                                        attributeModel.DefaultValue = enteredText[0];
                                }
                            }
                            break;
                        case AttributeControlType.Datepicker:
                        case AttributeControlType.ColorSquares:
                        case AttributeControlType.ImageSquares:
                        case AttributeControlType.FileUpload:
                        default:
                            //not supported attribute control types
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Prepare customer attribute models
        /// </summary>
        /// <param name="customer">Customer</param>
        protected virtual string GetCustomerWPayID(Customer customer)
        {
            string wpayID = string.Empty;
            //set already selected attributes
            if (customer != null)
            {
                //get available customer attributes
                var attribute = _customerAttributeService.GetAllCustomerAttributes().Where(ca => ca.Name.Equals("WPayID")).First();
                var attributeModel = new CustomerModel.CustomerAttributeModel
                {
                    Id = attribute.Id,
                    Name = attribute.Name,
                    IsRequired = attribute.IsRequired,
                    AttributeControlType = attribute.AttributeControlType
                };

                if (attribute.ShouldHaveValues())
                {
                    //values
                    var attributeValues = _customerAttributeService.GetCustomerAttributeValues(attribute.Id);
                    foreach (var attributeValue in attributeValues)
                    {
                        var attributeValueModel = new CustomerModel.CustomerAttributeValueModel
                        {
                            Id = attributeValue.Id,
                            Name = attributeValue.Name,
                            IsPreSelected = attributeValue.IsPreSelected
                        };
                        attributeModel.Values.Add(attributeValueModel);
                    }
                }
                
                var selectedCustomerAttributes = _genericAttributeService.GetAttribute<string>(customer, NopCustomerDefaults.CustomCustomerAttributes);
                if (!string.IsNullOrEmpty(selectedCustomerAttributes))
                {
                    var enteredText = _customerAttributeParser.ParseValues(selectedCustomerAttributes, attribute.Id);
                    if (enteredText.Any())
                    {
                        wpayID = enteredText[0];
                        attributeModel.DefaultValue = enteredText[0];
                    }


                }
            }

            return wpayID;
        }

        #endregion

    }
}