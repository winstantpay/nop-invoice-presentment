﻿using Nop.Core;

namespace Nop.Plugin.Widgets.WinstantPayInvoice
{
    /// <summary>
    /// Represents plugin constants
    /// </summary>
    public class WinstantPayInvoiceDefaults
    {
        /// <summary>
        /// Name of the view component to display plugin in public store
        /// </summary>
        public const string VIEW_COMPONENT_NAME = "WidgetsWinstantPayInvoice";

        /// <summary>
        /// WinstantPay Invoice system name
        /// </summary>
        public static string SystemName => "Widgets.WinstantPayInvoice";

        /// <summary>
        /// User agent used to request Square services
        /// </summary>
        public static string UserAgent => $"nopCommerce-{NopVersion.CurrentVersion}";

        /// <summary>
        /// The Integration ID should be added in the charge endpoint in order to correctly track the profit generated by the merchants using the Square payment plugin
        /// </summary>
        public static string IntegrationId => "sqi_4efb0346e2ef4b1375319dcd6e9977c0";

        /// <summary>
        /// One page checkout route name
        /// </summary>
        public static string OnePageCheckoutRouteName => "CheckoutOnePage";

        /// <summary>
        /// Path to the Square payment form js script
        /// </summary>
        public static string PaymentFormScriptPath => "https://js.squareup.com/v2/paymentform";

        /// <summary>
        /// Key of the attribute to store Square customer identifier
        /// </summary>
        public static string CustomerIdAttribute => "SquareCustomerId";

        /// <summary>
        /// Name of the route to the access token callback
        /// </summary>
        public static string CheckInvoiceStatusRoute => "Plugin.Widgets.WinstantPayInvoice.Status";

        /// <summary>
        /// Name of the renew access token schedule task
        /// </summary>
        public static string CheckWinstantPayInvoiceStatusTaskName => "Check invoice status on WinstantPay";

        /// <summary>
        /// Type of the renew access token schedule task
        /// </summary>
        public static string CheckWinstantPayInvoiceStatusTask => "Nop.Plugin.Widgets.WinstantPayInvoice.Services.CheckWinstantPayInvoiceStatusTask";

        /// <summary>
        /// Default access token renewal period in seconds
        /// </summary>
        public static int DefaultCheckWinstantPayInvoiceStatusPeriod => 300;

        /// <summary>
        /// Max access token renewal period in days
        /// </summary>
        public static int AccessTokenRenewalPeriodMax => 30;

        /// <summary>
        /// Sandbox credentials should start with this prefix
        /// </summary>
        public static string SandboxCredentialsPrefix => "sandbox-";

        /// <summary>
        /// Note passed for each payment transaction
        /// </summary>
        /// <remarks>
        /// {0} : Order Guid
        /// </remarks>
        public static string InvoiceNote => "nopCommerce: {0}";
    }
}