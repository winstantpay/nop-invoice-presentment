﻿using Nop.Core.Configuration;

namespace Nop.Plugin.Widgets.WinstantPayInvoice
{
    public class WinstantPayInvoiceSettings : ISettings
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string WPayId { get; set; }
        public string Domain { get; set; }
        public string WebServiceUrl { get; set; }
        public bool IsShowViewInvoiceButtonOnCustomerOrderDetailPage { get; set; }
        public bool IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage { get; set; }
        public bool IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage { get; set; }
        public bool IsSendInvoiceToWinstantPayWhenOrderPlaced { get; set; }
        public string Company { get; set; }
        public string StreetAddress { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Country { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
    }
}