﻿using System;
using Newtonsoft.Json;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Models
{
    public class NewInvoiceModel
    {
        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("invoiceNumber")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("receiverCompany")]
        public string ReceiverCompany { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }

        [JsonProperty("wpyId")]
        public string WpyId { get; set; }

        [JsonProperty("receiverWpyId")]
        public string ReceiverWpyId { get; set; }

        [JsonProperty("receiverEmail")]
        public string ReceiverEmail { get; set; }

        [JsonProperty("amount")]
        public int Amount { get; set; }

        [JsonProperty("dueDate")]
        public DateTime DueDate { get; set; }

        [JsonProperty("ccy")]
        public string Ccy { get; set; }

        [JsonProperty("invoiceFile")]
        public Byte[] InvoiceFile { get; set; }

        [JsonProperty("streetAddress")]
        public string StreetAddress { get; set; }

        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("receiverStreetAddress")]
        public string ReceiverStreetAddress { get; set; }

        [JsonProperty("receiverPostalCode")]
        public string ReceiverPostalCode { get; set; }

        [JsonProperty("receiverCity")]
        public string ReceiverCity { get; set; }

        [JsonProperty("receiverCountry")]
        public string ReceiverCountry { get; set; }

        [JsonProperty("receiverState")]
        public string ReceiverState { get; set; }

        [JsonProperty("receiverPhone")]
        public string ReceiverPhone { get; set; }

        [JsonProperty("receiverCustomerId")]
        public string ReceiverCustomerId { get; set; }
    }
}
