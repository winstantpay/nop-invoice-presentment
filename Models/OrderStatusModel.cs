﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Models
{
    public class OrderStatusModel
    {
        public int OrderId { get; set; }
        public WinstantPayInvoiceStatus Status { get; set; }
    }

    /// <summary>
    /// Represents a WinstantPay invoice status enumeration
    /// </summary>
    public enum WinstantPayInvoiceStatus
    {
        /// <summary>
        /// Rejected
        /// </summary>
        Unknown = -1,

        /// <summary>
        /// Rejected
        /// </summary>
        Rejected = 0,

        /// <summary>
        /// Cancelled
        /// </summary>
        Cancelled = 1,

        /// <summary>
        /// Presented
        /// </summary>
        Presented = 2,

        /// <summary>
        /// Accepted
        /// </summary>
        Accepted = 3,

        /// <summary>
        /// PartiallyPaid
        /// </summary>
        PartiallyPaid = 4,

        /// <summary>
        /// Paid
        /// </summary>
        Paid = 5
    }
}