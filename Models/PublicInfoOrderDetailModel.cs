﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Models
{
    public class PublicInfoOrderDetailModel : BaseNopModel
    {
        public string InvoicePublicUrl { get; set; }
        public bool IsInvoiceExisted { get; set; }
    }
}