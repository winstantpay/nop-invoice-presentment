﻿using System.ComponentModel.DataAnnotations;
using Nop.Web.Framework.Mvc.ModelBinding;
using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Models
{
    public class ConfigurationModel : BaseNopModel
    {
        public int ActiveStoreScopeConfiguration { get; set; }
        
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.Username")]
        public string Username { get; set; }
        public bool Username_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.Password")]
        [DataType(DataType.Password)]
        [NoTrim]
        public string Password { get; set; }
        public bool Password_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.WPayId")]
        public string WPayId { get; set; }
        public bool WPayId_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.Domain")]
        public string Domain { get; set; }
        public bool Domain_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.WebServiceUrl")]
        public string WebServiceUrl { get; set; }
        public bool WebServiceUrl_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.IsShowViewInvoiceButtonOnCustomerOrderDetailPage")]
        public bool IsShowViewInvoiceButtonOnCustomerOrderDetailPage { get; set; }
        public bool IsShowViewInvoiceButtonOnCustomerOrderDetailPage_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage")]
        public bool IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage { get; set; }
        public bool IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage")]
        public bool IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage { get; set; }
        public bool IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.IsSendInvoiceToWinstantPayWhenOrderPlaced")]
        public bool IsSendInvoiceToWinstantPayWhenOrderPlaced { get; set; }
        public bool IsSendInvoiceToWinstantPayWhenOrderPlaced_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.Company")]
        public string Company { get; set; }
        public bool Company_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.StreetAddress")]
        public string StreetAddress { get; set; }
        public bool StreetAddress_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.PostalCode")]
        public string PostalCode { get; set; }
        public bool PostalCode_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.City")]
        public string City { get; set; }
        public bool City_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.Country")]
        public string Country { get; set; }
        public bool Country_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.State")]
        public string State { get; set; }
        public bool State_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.Email")]
        public string Email { get; set; }
        public bool Email_OverrideForStore { get; set; }
        [NopResourceDisplayName("Plugins.Widgets.WinstantPayInvoice.Phone")]
        public string Phone { get; set; }
        public bool Phone_OverrideForStore { get; set; }
    }
}