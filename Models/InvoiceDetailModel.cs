﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Models
{
    public class InvoiceDetailModel
    {    
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("createdAt")]
        public DateTime CreatedAt { get; set; }

        [JsonProperty("updatedAt")]
        public DateTime? UpdatedAt { get; set; }

        [JsonProperty("createdBy")]
        public string CreatedBy { get; set; }

        [JsonProperty("updatedBy")]
        public string UpdatedBy { get; set; }

        [JsonProperty("invoiceNumber")]
        public string InvoiceNumber { get; set; }

        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("date")]
        public DateTime Date { get; set; }

        [JsonProperty("dueDate")]
        public DateTime DueDate { get; set; }

        [JsonProperty("fileAttachmentId")]
        public string FileAttachmentId { get; set; }

        [JsonProperty("amount")]
        public decimal Amount { get; set; }

        [JsonProperty("ccy")]
        public string Ccy { get; set; }

        [JsonProperty("wpyId")]
        public string WpyId { get; set; }

        [JsonProperty("paymentId")]
        public string PaymentId { get; set; }

        [JsonProperty("company")]
        public string Company { get; set; }
        [JsonProperty("streetAddress")]
        public string StreetAddress { get; set; }
        [JsonProperty("postalCode")]
        public string PostalCode { get; set; }
        [JsonProperty("city")]
        public string City { get; set; }
        [JsonProperty("country")]
        public string Country { get; set; }
        [JsonProperty("state")]
        public string State { get; set; }
        [JsonProperty("email")]
        public string Email { get; set; }
        [JsonProperty("phone")]
        public string Phone { get; set; }

        [JsonProperty("customerId")]
        public string CustomerId { get; set; }

        [JsonProperty("receiverWpyId")]
        public string ReceiverWpyId { get; set; }

        [JsonProperty("receiverEmail")]
        public string ReceiverEmail { get; set; }

        [JsonProperty("receiverCompany")]
        public string ReceiverCompany { get; set; }

        [JsonProperty("receiverCustomerId")]
        public string ReceiverCustomerId { get; set; }

        [JsonProperty("receiverStreetAddress")]
        public string ReceiverStreetAddress { get; set; }
        [JsonProperty("receiverPostalCode")]
        public string ReceiverPostalCode { get; set; }
        [JsonProperty("receiverCity")]
        public string ReceiverCity { get; set; }
        [JsonProperty("receiverCountry")]
        public string ReceiverCountry { get; set; }
        [JsonProperty("receiverState")]
        public string ReceiverState { get; set; }
        [JsonProperty("receiverPhone")]
        public string ReceiverPhone { get; set; }
    }

    public class InvoiceDetailResultModel
    {
        [JsonProperty("data")]
        public InvoiceDetailModel Data { get; set; }
        [JsonProperty("message")]
        public string Message { get; set; }
        [JsonProperty("translationKey")]
        public string TranslationKey { get; set; }
    }

}
