﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Models
{
    public class WinstantPayLoginResultModel
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("oldWalletToken")]
        public string OldWalletToken { get; set; }
    }
}
