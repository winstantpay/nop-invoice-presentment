﻿using Nop.Web.Framework.Models;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Models
{
    public class PublicInfoModel : BaseNopModel
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string WebServiceUrl { get; set; }
        public int OrderId { get; set; }
        public bool IsInvoiceNumberExisted { get; set; }
        public bool IsAdminOrdersList { get; set; }
        public bool IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage { get; set; }
        public bool IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage { get; set; }
        public bool IsSendInvoiceToWinstantPayWhenOrderPlaced { get; set; }
        public string Company { get; set; }
    }
}