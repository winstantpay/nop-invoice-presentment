﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;
using Nop.Web.Framework;
using Nop.Web.Framework.Mvc.Routing;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Infrastructure
{
    /// <summary>
    /// Represents plugin route provider
    /// </summary>
    public class RouteProvider : IRouteProvider
    {
        /// <summary>
        /// Register routes
        /// </summary>
        /// <param name="routeBuilder">Route builder</param>
        public void RegisterRoutes(IRouteBuilder routeBuilder)
        {
            routeBuilder.MapRoute("Plugin.Widgets.WinstantPayInvoice.Send", "Plugins/Invoice/Send",
                new { controller = "WidgetsWinstantPayInvoice", action = "SendInvoice", area = AreaNames.Admin, }                
            );
            
            routeBuilder.MapRoute("Plugin.Widgets.WinstantPayInvoice.Detail", "Plugins/Invoice/Detail",
                new { controller = "WidgetsWinstantPayInvoice", action = "GetInvoiceDetail", area = AreaNames.Admin, }                
            );

            routeBuilder.MapRoute("Plugin.Widgets.WinstantPayInvoice.Create", "Plugins/Invoice/Create",
                new { controller = "WidgetsWinstantPayInvoice", action = "CreateInvoice", area = AreaNames.Admin, }
            );

            routeBuilder.MapRoute("Plugin.Widgets.WinstantPayInvoice.GetStatus", "Plugins/Invoice/Status",
                new { controller = "WidgetsWinstantPayInvoice", action = "GetInvoiceStatus", area = AreaNames.Admin, }
            );

            routeBuilder.MapRoute("Plugin.Widgets.WinstantPayInvoice.CreateMultipleInvoices", "Plugins/Invoice/CreateMultiple",
                new { controller = "WidgetsWinstantPayInvoice", action = "CreateMultipleInvoices", area = AreaNames.Admin, }
            );

            routeBuilder.MapRoute("Admin.Order.Edit", "Admin/Order/Edit",
                new { controller = "Order", action = "Edit", area = AreaNames.Admin, }
            );

            routeBuilder.MapRoute("Plugin.Widgets.WinstantPayInvoice.ViewInvoice", "Plugins/Invoice/View",
                new { controller = "WidgetsWinstantPayInvoice", action = "ViewInvoice", area = AreaNames.Admin, }
            );
        }

        /// <summary>
        /// Gets a priority of route provider
        /// </summary>
        public int Priority => 100;
    }
}