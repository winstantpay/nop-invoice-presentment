﻿using System;
using Microsoft.AspNetCore.Mvc;
using Nop.Core;
using Nop.Plugin.Widgets.WinstantPayInvoice.Models;
using Nop.Web.Framework.Components;
using Nop.Web.Areas.Admin.Models.Orders;
using Nop.Plugin.Widgets.WinstantPayInvoice.Services;
using Nop.Services.Logging;
using Newtonsoft.Json;
using Nop.Web.Framework.Infrastructure;
using Nop.Web.Models.Order;
using System.Threading.Tasks;
using Nop.Services.Security;

namespace Nop.Plugin.Widgets.WinstantPayInvoice.Components
{
    [ViewComponent(Name = "WidgetsWinstantPayInvoice")]
    public class WidgetsWinstantPayInvoiceViewComponent : NopViewComponent
    {
        private readonly ILogger _logger;
        private readonly WinstantPayHttpClient _winstantPayHttpClient;
        private readonly WinstantPayInvoiceSettings _invoiceSettings;
        private readonly WinstantPayInvoiceManager _winstantPayInvoiceManager;
        private readonly IEncryptionService _encryptionService;

        public WidgetsWinstantPayInvoiceViewComponent(IStoreContext storeContext,
            ILogger logger,
            WinstantPayHttpClient winstantPayHttpClient,
            WinstantPayInvoiceSettings invoiceSettings,
            WinstantPayInvoiceManager winstantPayInvoiceManager,
            IEncryptionService encryptionService
            )
        {
            _logger = logger;
            _winstantPayHttpClient = winstantPayHttpClient;            
            _invoiceSettings = invoiceSettings;
            _winstantPayInvoiceManager = winstantPayInvoiceManager;
            _encryptionService = encryptionService;
        }

        public IViewComponentResult Invoke(string widgetZone, object additionalData)
        {
            var model = new PublicInfoModel();
            var loginResult = LoginToWinstantPay();
            if (loginResult == null || loginResult.Token == null)
            {
                // Write log, Login failed
                _logger.Error("WidgetsWinstantPayInvoiceViewComponent - Invoke - Login to WinstantPay Failed, disable all functions");
                return Content("");
            }

            if (widgetZone.Equals(AdminWidgetZones.OrderDetailsButtons))
            {
                // If in admin order details
                var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
                var orderModel = (OrderModel) additionalData;
                model.Username = _invoiceSettings.Username;
                model.Password = decryptedPassword;
                model.WebServiceUrl = _invoiceSettings.WebServiceUrl;
                model.OrderId = orderModel.Id;
                model.IsInvoiceNumberExisted = IsInvoiceNumberExisted(orderModel.Id, loginResult.Token).Result;
                model.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage = _invoiceSettings.IsShowGetInvoiceStatusButtonOnAdminOrderDetailPage;
                model.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage = _invoiceSettings.IsShowSendInvoiceToWinstantPayButtonOnAdminOrderDetailPage;

                return View("~/Plugins/Widgets.WinstantPayInvoice/Views/PublicInfoAdminOrderDetails.cshtml", model);
            }
            else if (widgetZone.Equals(PublicWidgetZones.OrderDetailsPageOverview) && _invoiceSettings.IsShowViewInvoiceButtonOnCustomerOrderDetailPage)
            {
                // If in public order details page
                var publicInfoOrderDetailsModel = new PublicInfoOrderDetailModel();
                var orderDetailsModel = (OrderDetailsModel)additionalData;
                publicInfoOrderDetailsModel.IsInvoiceExisted = _winstantPayInvoiceManager.IsInvoiceNumberExisted(_winstantPayInvoiceManager.GenerateInvoiceNumber(orderDetailsModel.Id), loginResult.Token);
                if (publicInfoOrderDetailsModel.IsInvoiceExisted)
                {
                    // If invoice existed, get public URL for viewing WinstantPay invoice
                    publicInfoOrderDetailsModel.InvoicePublicUrl = _winstantPayInvoiceManager.GetInvoicePublicUrl(_winstantPayInvoiceManager.GenerateInvoiceNumber(orderDetailsModel.Id), loginResult.Token).Result;
                }

                return View("~/Plugins/Widgets.WinstantPayInvoice/Views/PublicInfoOrderDetail.cshtml", publicInfoOrderDetailsModel);
            }
            else
                return Content("");
        }

        private WinstantPayLoginResultModel LoginToWinstantPay()
        {
            var loginResultModel = new WinstantPayLoginResultModel();
            try
            {
                // Decrypt password
                var decryptedPassword = _encryptionService.DecryptText(_invoiceSettings.Password);
                var loginModel = new WinstantPayLoginModel
                {
                    Username = _invoiceSettings.Username,
                    Password = decryptedPassword,
                    Domain = _invoiceSettings.Domain
                };

                // Login to WinstantPay
                var loginResponse = _winstantPayHttpClient.LoginWinstantPayAsync(_invoiceSettings.WebServiceUrl, loginModel);
                if (loginResponse != null && loginResponse.Result != null)
                {
                    // Login successfull
                    loginResultModel = loginResponse.Result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("LoginToWinstantPay - Login Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return loginResultModel;
        }

        private bool IsInvoiceNumberExisted(int orderId)
        {
            var isExisted = false;

            try
            {
                // Login to WinstantPay
                var loginResult = LoginToWinstantPay();
                if (loginResult != null && loginResult.Token != null)
                {
                    var response = _winstantPayHttpClient.IsInvoiceNumberExistsAsync(_invoiceSettings.WebServiceUrl, orderId.ToString(), loginResult.Token);
                    isExisted = response.Result;
                }
            }
            catch (Exception ex)
            {
                _logger.Error("WidgetsWinstantPayInvoiceViewComponent - GetInvoiceByNumber Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return isExisted;
        }

        private async Task<bool> IsInvoiceNumberExisted(int orderId, string accessToken)
        {
            var isExisted = true;

            try
            {
                isExisted = await _winstantPayInvoiceManager.IsInvoiceNumberExistedAsync(_winstantPayInvoiceManager.GenerateInvoiceNumber(orderId), accessToken);
            }
            catch (Exception ex)
            {
                _logger.Error("WidgetsWinstantPayInvoiceViewComponent - GetInvoiceByNumber Failed, ex: " + JsonConvert.SerializeObject(ex));
            }

            return isExisted;
        }
    }
}
